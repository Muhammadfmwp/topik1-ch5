const express = require ("express");

const app = express();

app.use(express.json());
app.set("view engine", "ejs");

const moviesServices = require("./services/moviesServices");

const isAvailable = (req, res, next) => {
    const data = [...moviesServices.data];
    const { id } = req.query;
    const isAvailable = data.find((row) => row.id == id);
    valid = Boolean(isAvailable);
    if(valid === true){
        next();
        return;
    }

    res.status(404).send("Id tidak ditemukan");
};

const isAdmin = (req, res, next) => {
    const {isAdmin} = req.query;

    if(isAdmin == 1){
        next();
        return;
    }

    res.status(401).send("Hak akses ditolak");
};



app.get("/movies", moviesServices.getMovies);
app.get("/movie", isAvailable, moviesServices.getMovie);
app.post("/movie", moviesServices.createMovie);
app.delete("/movie", isAdmin, moviesServices.deleteMovie);
app.put("/movie", isAdmin, moviesServices.updateMovie);
app.delete("/movies", isAdmin, moviesServices.deleteMovies);

app.listen(5000, function () {
    console.log("Server started on port 5000...")
} )