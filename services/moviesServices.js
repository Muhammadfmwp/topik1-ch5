const data = [
    { id: 1, title: "Morbius", studio: "Marvel", year: 2022 },
    { id: 2, title: "Dr. Strange", studio: "Marvel", year: 2016 },
    { id: 3, title: "Amazing Spiderman", studio: "Marvel", year: 2012 },
    { id: 4, title: "Iron Man", studio: "Marvel", year: 2008 },
    { id: 5, title: "Venom", studio: "Marvel", year: 2018 },
    { id: 6, title: "Cat Woman", studio: "DC", year: 2004 },
    { id: 7, title: "Batman Begins", studio: "DC", year: 2005 },
    { id:8, title: "Superman Return", studio: "DC", year: 2006 },
    { id:9, title: "Mulan", studio: "Walt Disney", year: 2020 },
    { id:10, title: "Aladdin", studio: "Walt Disney", year: 2019 },
];


const getMovies = (req,res) => {
    res.status(200).send(data);
};

const getMovie = (req,res) => {
    const { id } = req.query;
    const value = data.find((row) => row.id == id);

    res.status(200).send(value);

};

const createMovie = (req, res) => {
    data.push(req.body);

    res.status(201).send("Data created");
};

const updateMovie = (req, res) => {
    const { id } = req.query;
    const index = data.findIndex((row) => row.id == id);

    data[index] = {
        ...data[index],
        ...req.body,
    };
};

const deleteMovie = (req,res) => {
  
    const { id } = req.query;
    const index = data.findIndex((row) => row.id ==id);

    data.splice(index, 1);

    res.status(200).send(`Data with id ${id} was deleted`);

}

const deleteMovies = (req,res) => {

    const { id } = req.query;
    console.log(id);


    for(let i of id){
        const index = data.findIndex((row) => row.id == i);
        data.splice(index, 1);
    
        }
        res.status(200).send(`Data with id ${id} were deleted`);
}

module.exports = {data, getMovies, getMovie, createMovie, updateMovie, deleteMovie, deleteMovies}